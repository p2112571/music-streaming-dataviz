#!/usr/bin/python3

from IPython.display import display, Javascript

load_d3 = """
var head = document.getElementsByTagName('head')[0];
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = 'https://d3js.org/d3.v7.min.js';
    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    //script.onreadystatechange = callback;
    //script.onload = callback;
    // Fire the loading
    head.appendChild(script);
"""

fonction_js = """
window.dessiner = function(element, donnees_geo){
let d3 = window.d3;
let maxi = function(dat, t){
    let d = dat.geo;
    let T = d3.max(d, (x) => x[t]);
    return T;
};

let mini = function(dat, t){
    let d = dat.geo;
    let T = d3.min(d, (x) => x[t]);
    return T;
};

let max_long = d3.max(donnees_geo, (d) => maxi(d, "lng"));
let min_long = d3.min(donnees_geo, (d) => mini(d, "lng"));
let max_lat = d3.max(donnees_geo, (d) => maxi(d, "lat"));
let min_lat = d3.min(donnees_geo, (d) => mini(d, "lat"));

let largeur = max_long - min_long;
let hauteur = max_lat - min_lat;


let width = 800;
let height = 800 * hauteur/largeur;

let svg = d3.select("#visu-barcelone")
			.append("svg")
			.attr("width", width)
			.attr("height", height)
			.append("g");


const Ech_x = d3.scaleLinear()
        .range([0, width])
        .domain([min_long, max_long]);

const Ech_y = d3.scaleLinear()
        .range([0, height])
        .domain([max_lat, min_lat]);

svg.selectAll(".sommet")
   .data(donnees_geo)
   .enter()
   //.append("g")
   .each((d) => {let points = d.geo;
                 let p0 = points[points.length -1];
                 for (p of points)
                 {
                    //d3.select(this)
                    svg
                    .append("line")
                    .attr("x1", Ech_x(p0.lng))
                    .attr("y1", Ech_y(p0.lat))
                    .attr("x2", Ech_x(p.lng))
                    .attr("y2", Ech_y(p.lat))
                    .attr("stroke", "black");
                    p0 = p;
                 }
                });
};
"""

def preparer_d3js():
    #display(Javascript("require.config({paths: {d3: 'https://d3js.org/d3.v7.min.js'}});"))
    display(Javascript(load_d3))


